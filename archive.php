<?php get_header(); ?>
<section id="archive-posts" class="row posts-gallery">
	<?php the_archive_title( '<h1 class="col-md-12 title-upper">', '</h1>' ); ?>
	<?php if(have_posts()): while(have_posts()): the_post(); ?>
		<article class="col-md-6">
			<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
				<header>
					<h2>
						<?php the_title(); ?>
					</h2>
				</header>
				<figure>
					<?php the_post_thumbnail('thumbnail'); ?>
					<div class="color"></div>
					<span class="glyphicon glyphicon-zoom-in"></span>
				</figure>
			</a>
			<footer>
				<?php cat_breadcrumb(); ?>
			</footer>
		</article>
	<?php endwhile; ?>
		<?php pagination(); ?>
	<?php else: ?>
	<article class="container-text text-center">Nenhum post encontrado!</article>
	<?php endif;?>
</section>
<?php get_footer(); ?>