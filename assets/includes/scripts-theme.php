<?php

function wp_scripts_theme() {
	wp_enqueue_style( 'app', get_template_directory_uri() . '/assets/css/app.min.css' );
	wp_enqueue_style( 'style', get_stylesheet_uri() );

	//Carregando no footer
	wp_enqueue_script('app-js', get_template_directory_uri().'/assets/js/app.min.js', array('jquery'), '', true );
	wp_enqueue_script('functions-js', get_template_directory_uri().'/assets/js/functions.js', array('jquery'), '', true );
	//wp_enqueue_script('google-maps-api', 'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false', array('jquery'), '', true );
}
add_action( 'wp_enqueue_scripts', 'wp_scripts_theme' );
