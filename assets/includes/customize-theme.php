<?php 
function customize_register_theme( $wp_customize ){
	$themeName = 'ltco-flat';

	/* SESSÂO HEADER */
	/*$wp_customize->add_section('themeCustom-section-header', array(
		'title'				=> __('Logo Animate', $themeName),
		'description'		=> '',
	));*/
	
	/* LOGO */
	$wp_customize->add_setting('custom_logo_mobile', array(
		'default'			=> '',
	));
	
	$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'custom_logo_mobile', array(
		'label'				=> __('Logo Animate', $themeName),
		'section'			=> 'title_tagline',
		'setting'			=> 'custom_logo_mobile',
	)));

	/*$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial( 'blogname', array(
			'selector' => '.site-title a',
			'container_inclusive' => false,
			'render_callback' => 'customize_partial_blogname',
		) );
		$wp_customize->selective_refresh->add_partial( 'blogdescription', array(
			'selector' => '.site-description',
			'container_inclusive' => false,
			'render_callback' => 'customize_partial_blogdescription',
		) );
	}
	$wp_customize->remove_control( 'header_textcolor' );
}

function customize_partial_blogname() {
	bloginfo( 'name' );
}

function customize_partial_blogdescription() {
	bloginfo( 'description' );*/
}

add_action( 'customize_register' , 'customize_register_theme', 11 );