<?php 

function widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'ltco-flat' ),
		'id'            => 'sidebar',
		'before_widget' => '<section id="%1$s" class="widget col-md-12 %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title col-md-12">',
		'after_title'   => '</h3>',
	));

	register_sidebar( array(
		'name'          => __( 'Footer 1', 'ltco-flat' ),
		'id'            => 'footer-1',
		'before_widget' => '<section id="%1$s" class="widget col-md-12 %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title col-md-12">',
		'after_title'   => '</h3>',
	));

	register_sidebar( array(
		'name'          => __( 'Footer 2', 'ltco-flat' ),
		'id'            => 'footer-2',
		'before_widget' => '<section id="%1$s" class="widget col-md-12 %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title col-md-12">',
		'after_title'   => '</h3>',
	));

	register_sidebar( array(
		'name'          => __( 'Footer 3', 'ltco-flat' ),
		'id'            => 'footer-3',
		'before_widget' => '<section id="%1$s" class="widget col-md-12 %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title col-md-12">',
		'after_title'   => '</h3>',
	));

	register_sidebar( array(
		'name'          => __( 'Footer 4', 'ltco-flat' ),
		'id'            => 'footer-4',
		'before_widget' => '<section id="%1$s" class="widget col-md-12 %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title col-md-12">',
		'after_title'   => '</h3>',
	));
}
add_action( 'widgets_init', 'widgets_init' );
