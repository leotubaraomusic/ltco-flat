jQuery(document).ready(function($) {
	var btnSearch = $('.btn-search'),
		formSearch = $('.search-form'),
		inputSearch = $('.search-form input[type="search"]');

	btnSearch.click(function(){
		formSearch.toggleClass("open");
		inputSearch.focus();
	});
	
	var offset = 300,
	offset_opacity = 1200,
	scroll_top_duration = 700,
	$back_to_top = $('.top');

	$(window).scroll(function(){
		( $(this).scrollTop() > offset ) ? $back_to_top.addClass('visible') : $back_to_top.removeClass('visible fade-out');
		/*if( $(this).scrollTop() > offset_opacity ) { 
			$back_to_top.addClass('cd-fade-out');
		}*/
	});

	$back_to_top.on('click', function(event){
		event.preventDefault();
		$('body,html').animate({
			scrollTop: 0,
		}, scroll_top_duration);
	});

	function logoAdjust(){
		var logo = $(".logo-mobile"),
			logoEmbed = $(".logo-mobile .emblem-inner"),
			logoImg = $(".logo-mobile img"),
			sizeHeight = logoImg.height();

		logo.css("height", sizeHeight);
		logoEmbed.css("height", sizeHeight);
	}

	logoAdjust();
	
	$(window).resize(function(){
		logoAdjust();
	});
});