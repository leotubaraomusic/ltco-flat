<?php get_header(); ?>
<section class="row">
<?php if(have_posts()): while(have_posts()): the_post(); ?>
	<header>
		<h1 class="col-md-12 title-upper"><?php the_title();?></h1>
	</header>
	<article id="post-<?php the_ID(); ?>" <?php post_class('col-md-12'); ?>>
		<?php the_content();?>
	</article>
<?php endwhile; endif;?>
</section>
<?php get_footer(); ?>