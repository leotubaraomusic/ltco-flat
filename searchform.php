<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<label class="search-input col-md-10 col-sm-11 col-xs-10">
		<input type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Pesquisar &hellip;', 'placeholder', 'ltco-flat' ); ?>" value="<?php echo get_search_query(); ?>" name="s" />
	</label>
	<button type="submit" class="search-submit col-md-2 col-sm-1 col-xs-2">
		<span class="glyphicon glyphicon-search"></span>
		<?php echo _x( '', 'submit button', 'ltco-flat' ); ?>
	</button>
</form>