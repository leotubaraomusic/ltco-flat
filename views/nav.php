<nav class="navbar navbar-default">
	<div class="navbar-brand">
		<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
			<?php bloginfo( "name" ); ?>
		</a>
	</div>
	<div class="navbar-header">
		<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
	</div>
	<?php
		wp_nav_menu( array(
			'menu'				=> 'Menu Principal',
			'theme_location'	=> 'ltco-flat',
			'depth'				=> 2,
			'container'			=> 'div',
			'container_class'	=> 'collapse navbar-collapse',
			'container_id'		=> 'navbar-collapse-1',
			'menu_class'		=> 'nav navbar-nav',
			'fallback_cb'		=> 'wp_bootstrap_navwalker::fallback',
			'walker'			=> new wp_bootstrap_navwalker())
		);
	?>
</nav>