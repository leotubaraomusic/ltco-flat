<?php get_header(); ?>
<section class="error-404 not-found">
	<header>
		<h1 class="col-md-12 title-upper"><?php _e( 'Ops! Essa página não pode ser encontrada.', 'ltco-flat' ); ?></h1>
	</header>
	<article class="content">
		<p><?php _e( 'Parece que nada foi encontrado neste local. Talvez tente uma pesquisa?', 'ltco-flat' ); ?></p>
		<?php get_search_form(); ?>
	</article>
</section>
<?php get_footer(); ?>