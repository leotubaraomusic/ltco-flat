<!DOCTYPE html>
<html lang="pt-br">
<head>
	<?php
		/*if(is_single() || is_page() || is_category() || is_home()) {
			echo '<meta name="robots" content="all,noodp" />';
		}
		else if(is_archive()) {
			echo '<meta name="robots" content="noarchive,noodp" />';
		}
		else if(is_search() || is_404()) {
			echo '<meta name="robots" content="noindex,noarchive" />';
		}*/
	?>
	<meta charset="UTF-8">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<meta name="theme-color" content="#C43F3A">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<!--<link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,700|Droid+Serif:400,700' rel='stylesheet' type='text/css'>-->
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<header class="header">
	<?php get_template_part('/views/nav');?>
	<?php get_search_form(); ?>
	<div data-jarallax='{"speed": -0.2}' class="jarallax" style="background-image: url(<?php parallax(); ?>);">
		<div class="color"></div>
		<div class="container logo-header">
			<div class="row">
				<div class="col-md-12">
				<?php themeCustom_the_custom_logo();?>
				</div>
			</div>
		</div>
	</div>
</header>
<main class="container">