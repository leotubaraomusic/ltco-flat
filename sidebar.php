<?php if ( is_active_sidebar( 'sidebar' )  ) : ?>
	<aside class="sidebar widget-area col-md-4">
		<?php dynamic_sidebar( 'sidebar' ); ?>
	</aside>
<?php endif; ?>