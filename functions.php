<?php

$themeName = 'ltco-flat';

/*==========================================
=            Thumbnails Customs            =
==========================================*/

add_theme_support( 'post-thumbnails' );
add_image_size( 'photo-about', 300, 300, true );

/*=====  End of Thumbnails Customs  ======*/

/*=======================================
=            Active Lightbox            =
=======================================*/

if (!function_exists('themeCustom_lightbox_gallery')) {
	function themeCustom_lightbox_gallery($string) {
		global $post;
		return preg_replace('/<a(.*?)href="(.*?).(bmp|gif|jpeg|jpg|png)"(.*?)>/i', '<a$1href="$2.$3" rel="lightbox">', $string);
	}
}

/*=====  End of Active Lightbox  ======*/

/*========================================
=            Class Responsive            =
========================================*/

if (!function_exists('bootstrap_responsive_images')) {
	function bootstrap_responsive_images( $html ){
		$classes = 'img-responsive'; // separated by spaces, e.g. 'img image-link'
		// check if there are already classes assigned to the anchor
		if ( preg_match('/<img.*? class="/', $html) ) {
			$html = preg_replace('/(<img.*? class=".*?)(".*?\/>)/', '$1 ' . $classes . ' $2', $html);
		} else {
			$html = preg_replace('/(<img.*?)(\/>)/', '$1 class="' . $classes . '" $2', $html);
		}
		// remove dimensions from images,, does not need it!
		$html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html );
		return $html;
	}
}

/*=====  End of Class Responsive  ======*/

/*============================
=            Menu            =
============================*/

/*----------  Register Menu  ----------*/

register_nav_menus( array(
    'primary' => __( 'Menu Principal', $themeName ),
));

/*----------  Active Menu Item  ----------*/

if (!function_exists('special_nav_class')) {
	function special_nav_class($classes, $item){
		if( in_array('current-menu-item', $classes) ){
			$classes[] = 'active ';
		}
		return $classes;
	}
}

/*=====  End of Menu  ======*/

/*======================================
=            Custom Excerpt            =
======================================*/

if (!function_exists('excerpt')) {
	function excerpt($limit) {
		$excerpt = explode(' ', get_the_excerpt(), $limit);
		if (count($excerpt)>=$limit) {
			array_pop($excerpt);
			$excerpt = implode(" ",$excerpt).'...';
		} else {
			$excerpt = implode(" ",$excerpt);
		}
		$excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
		return $excerpt;
	}
}

/*=====  End of Custom Excerpt  ======*/

/*=================================
=            Tag Title            =
=================================*/

if (!function_exists('themeCustom_wp_title')) {
	function themeCustom_wp_title( $title, $sep ) {
		global $paged, $page;

		if ( is_feed() ) {
			return $title;
		}

		// Add the site name.
		$title .= get_bloginfo( 'name', 'display' );

		// Add the site description for the home/front page.

		$site_description = get_bloginfo( 'description', 'display' );
		if ( $site_description && ( is_home() || is_front_page() ) ) {
			$title = "$title $sep $site_description";
		}

		// Add a page number if necessary.
		if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() ) {
			$title = "$title $sep " . sprintf( __( 'Page %s', $themeName ), max( $paged, $page ) );
		}

		return $title;
	}
}

/*=====  End of Tag Title  ======*/

/*================================
=            Parallax            =
================================*/

if (!function_exists('parallax')) {
	function parallax() {
		if(is_front_page() || is_home()){
			header_image();
		}
		else if(is_single() || is_page()) {
			if (has_post_thumbnail()) {
				the_post_thumbnail_url();
			} else {
				header_image();
			}
		}
		else if(is_category() || is_tag() || is_archive() || is_search() || is_404()) {
			header_image();
		}
	}
}

/*=====  End of Parallax  ======*/

/*==================================
=            Categories            =
==================================*/

if (!function_exists('cat_breadcrumb')) {
	function cat_breadcrumb() {
		$categories = get_the_category();
		$output = '';
		echo '<ul class="breadcrumb">';
		if ( ! empty( $categories ) ) {
			foreach( $categories as $category ) {
				$output .= '
					<li>
						<a href="' . esc_url( get_category_link( $category->term_id ) ) . '">
							' . esc_html( $category->name ) . '
						</a>
					</li>'
				;
			}
			echo trim( $output );
		}
		echo '</ul>';
	}
}

/*=====  End of Categories  ======*/

/*============================
=            Tags            =
============================*/

if (!function_exists('tag_breadcrumb')) {
	function tag_breadcrumb() {
		$tags = get_the_tags();
		$output = '';
		echo '<ul class="breadcrumb">';
		if ( ! empty( $tags ) ) {
			foreach( $tags as $tag ) {
				$output .= '
					<li>
						<a href="' . esc_url( get_tag_link( $tag->term_id ) ) . '">
							' . esc_html( $tag->name ) . '
						</a>
					</li>'
				;
			}
			echo trim( $output );
		}
		echo '</ul>';
	}
}

/*=====  End of Tags  ======*/

/*==================================
=            Pagination            =
==================================*/

if (!function_exists('pagination')) {
	function pagination() {
		global $wp_query;
		if ($wp_query->max_num_pages > 1) {
		echo '
		<div class="col-md-12 pages">
			<ul class="pager">
				<li class="previous">
					' . get_previous_posts_link( "Anterior" ) . '
				</li>
				<li class="next">
					' . get_next_posts_link( "Próximo", $wp_query->max_num_pages ) . '
				</li>
			</ul>
		</div>';
		}
	}
}

/*=====  End of Pagination  ======*/

/*=========================================
=            Pagination Single            =
=========================================*/

if (!function_exists('post_nav')) {
	function post_nav() {
		echo '
		<div class="after-post pages">
			<ul class="pager">
				<li class="previous">
					' . get_next_post_link( $format = '%link', $link =  '<span class="glyphicon glyphicon-menu-left"></span><span class="title">%title</span>' ) . '
				</li>
				<li class="next">
					' . get_previous_post_link( $format = '%link', $link = '<span class="title">%title</span><span class="glyphicon glyphicon-menu-right"></span>' ) . '
				</li>
			</ul>
		</div>';
	}
}
	
/*=====  End of Pagination Single  ======*/

/*====================================
=            Have Sidebar            =
====================================*/

if (!function_exists('classCol')) {
	function classCol(){
		if (is_active_sidebar('sidebar')){
			$value = '8';
		} else {
			$value = '12';
		}

		return $value;
	}
}

/*=====  End of Have Sidebar  ======*/

/*===========================================
=            Widget Class Footer            =
===========================================*/

if (!function_exists('classWidget')) {
	function classWidget(){
		if (is_active_sidebar('footer-4')){
			$value = 'widget-footer widget-area ';
		}
		return $value;
	}
}

/*=====  End of Widget Class Footer  ======*/

/*===================================
=            Icon Social            =
===================================*/

if (!function_exists('iconSocial')) {
	function iconSocial($iconSocial = "facebook", $linkSocial = "#" ){
		$link = $linkSocial;
		$icon = $iconSocial;

		if ($link || $icon) {
			echo '
			<a href="' . $link . '" target="_blank" class="icon-footer ' . $icon . '">
				<svg>
					<use xlink:href="' . get_template_directory_uri() . '/assets/images/svg/sprite.svg#' . $icon . '"></use>
				</svg>
			</a>';
		}
	}
}

/*=====  End of Icon Social  ======*/

if (!function_exists('get_custom_logo_mobile')) {
	function get_custom_logo_mobile() {
		$custom_logo_mobile_id = get_theme_mod( 'custom_logo_mobile' );

		if ( $custom_logo_mobile_id ) {
			$html = sprintf( '<a href="%1$s" class="custom-logo-link logo-mobile" rel="home" itemprop="url">%2$s</a>',
				esc_url( home_url( '/' )),
				wp_get_attachment_image( $custom_logo_mobile_id, 'full', false, array(
					'class'		=> 'custom-logo-mobile',
					'itemprop'	=> 'logo',
				))
			);
		}
		return apply_filters( 'get_custom_logo_mobile', $html );
	}
}

function the_custom_logo_mobile() {
	echo get_custom_logo_mobile();
}

/*=======================================
=            Themes Supports            =
=======================================*/

/*----------  Menu  ----------*/

add_theme_support('menus');

/*----------  HTML5  ----------*/

add_theme_support( 'html5', array(
	'search-form',
	'comment-form',
	'comment-list',
	'gallery',
	'caption',
));

/*----------  POST FORMATS  ----------*/

add_theme_support( 'post-formats', array(
	'aside',
	'image',
	'video',
	'quote',
	'link',
	'gallery',
	'status',
	'audio',
	'chat',
));

/*----------  Background Header  ----------*/

add_theme_support( 'custom-header', array(
	'random-default'		=> true,
	'width'					=> 1920,
	'height'				=> 1080,
	'flex-height'			=> true,
	'flex-width'			=> true,
	'header-text'			=> true,
	'uploads'				=> true,
));
/*----------  Logo  ----------*/

add_theme_support( 'custom-logo', array(
	'width'       => 420,
	'height'      => 420,
	'flex-height' => true,
));

if ( ! function_exists( 'themeCustom_the_custom_logo' )){
	function themeCustom_the_custom_logo() {
		if (wp_is_mobile() || !is_home() || !is_front_page()) {
			the_custom_logo();
		} else {
			echo '<a href="' . esc_url( home_url( '/' )) . '" class="custom-logo-mobile-link logo-mobile" rel="home" itemprop="url">
				<div class="emblem-inner">
					<img src="' . get_theme_mod( 'custom_logo_mobile' ) . '" class="custom-logo-mobile" itemprop="logo" />
				</div>
			</a>';
		}
	}
}

/*=====  End of Themes Supports  ======*/

/*====================================
=            Theme Filter            =
====================================*/

/*----------  Lightbox Custom  ----------*/

add_filter('the_content', 'themeCustom_lightbox_gallery');

/*----------  Remove Admin Bar  ----------*/

add_filter('show_admin_bar', '__return_false');

/*----------  Tag Title  ----------*/

add_filter( 'wp_title', 'themeCustom_wp_title', 10, 2 );

/*----------  Active Menu Item  ----------*/

add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);

/*----------  Bootstrap Responsive Image  ----------*/

add_filter( 'the_content','bootstrap_responsive_images',10 );
add_filter( 'post_thumbnail_html', 'bootstrap_responsive_images', 10 );

/*=====  End of Theme Filter  ======*/

/*================================================
=            Remove WordPress Version            =
================================================*/

remove_action('wp_head', 'wp_generator');

/*=====  End of Remove WordPress Version  ======*/

/*=======================================
=            Includes Theme            =
=======================================*/

/*----------  Menu Style Bootstrap  ----------*/

require_once('assets/includes/wp_bootstrap_navwalker.php');

/*----------  Styles and Scripts  ----------*/

require 'assets/includes/scripts-theme.php';

/*----------  Register Sidebar  ----------*/

require 'assets/includes/register-sidebar.php';

/*----------  Customize Theme  ----------*/

require 'assets/includes/customize-theme.php';

/*=====  End of Includes Theme  ======*/