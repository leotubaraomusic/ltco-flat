<?php get_header(); ?>
<section id="index-sobre" class="row">
<?php
	$args = array('post_type' => 'page', 'pagename' => 'sobre-mim');
	$myPosts = get_posts( $args );
	if ($myPosts) : foreach ($myPosts as $post) : setup_postdata( $post );
?>
	<h2 class="col-md-12 title-upper"><?php the_title();?></h2>
	<?php the_post_thumbnail('photo-about'); ?>
	<section class="col-md-12">
		<article class="content">
			<?php echo excerpt('30');?>
		</article>
	</section>
	<article class="col-md-<?php if(get_field('cv')){ echo '6';} else { echo '12';} ?>">
		<a class="btn btn-default" href="<?php echo esc_url( home_url( '/sobre' ) ); ?>" role="button">
			Leia <span class="glyphicon glyphicon glyphicon-plus" aria-hidden="true"></span>
		</a>
	</article>
	<?php if(get_field('cv')): ?>
	<article class="btn-col col-md-6">
		<a class="btn btn-default" href="<?php echo the_field('cv');?>" target="_blank" role="button">
			Currículo
		</a>
	</article>
	<?php endif; ?>
<?php endforeach; endif; wp_reset_postdata();?>
</section>
<section id="index-posts" class="row posts-gallery">
	<?php if(have_posts()):?>
		<h2 class="col-md-12 title-upper">Portfólio</h2>
	<?php endif; ?>
	<?php
		$args = array('post_type' => 'post', 'showposts' => 6);
		query_posts( $args );
		if(have_posts()): while(have_posts()): the_post();
	?>
		<article class="col-md-6">
			<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
				<header>
					<h3>
						<?php the_title(); ?>
					</h3>
				</header>
				<figure>
					<?php the_post_thumbnail('thumbnail'); ?>
					<div class="color"></div>
					<span class="glyphicon glyphicon-zoom-in"></span>
				</figure>
			</a>
			<footer>
				<?php cat_breadcrumb(); ?>
			</footer>
		</article>
	<?php endwhile; ?>
		<article class="col-md-12">
			<a class="btn btn-default" href="<?php echo esc_url( home_url( '/portfolio' ) ); ?>" role="button">
				Veja <span class="glyphicon glyphicon glyphicon-plus" aria-hidden="true"></span>
			</a>
		</article>
	<?php else: ?>
	<article class="container-text text-center">Nenhum post encontrado!</article>
	<?php endif; ?>
</section>
<?php get_footer(); ?>