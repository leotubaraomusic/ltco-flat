</main>
<footer class="footer">
	<section class="container">
		<div class="row">
			<?php the_custom_logo(); ?>
			<?php iconSocial('facebook','https://www.facebook.com/portfolioltco/'); ?>
		</div>
	</section>
	<section class="container">
			<strong>
				<span>&copy; 2016 <?php bloginfo('name') ?></span>
				<span>Todos os Direitos Reservados.</span>
			</strong>
	</section>
</footer>
<a href="javascript:void(0);" class="btn-search"><span class="glyphicon glyphicon-search"></span></a>
<a href="javascript:void(0);" class="top"><span class="glyphicon glyphicon-menu-up"></span></a>
<script type="text/javascript">jQuery(document).ready(function($) {$(".rss-widget-icon").attr("src", "<?php echo get_template_directory_uri(); ?>/assets/images/svg/rss.svg");});</script>
<?php wp_footer(); ?>
</body>
</html