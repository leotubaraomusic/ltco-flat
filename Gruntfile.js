module.exports = function (grunt) {
	grunt.initConfig({
		/*jshint: { // Verificar a qualidade dos arquivos JavaScripts.
			dist: {
				src: ['assets/js/app.min.js']
			}
		},*/
		cssmin: { // Minificar todos os arquivos ".css" em um só.
			all: {
				src: ['node_modules/normalize.css/normalize.css'],
				dest: 'assets/css/normalize.min.css'
			}
		},
		concat: {  // Concatenar arquivos em um só.
			bowercss: {
				stripBanners: true,
				src: [
					'assets/css/normalize.min.css',
					'bower_components/bootstrap/dist/css/bootstrap.min.css',
					'bower_components/lightbox2/dist/css/lightbox.min.css'
				],
				dest: 'assets/css/app.min.css'
			}/*,
			css: {
				src: ['assets/css/copyright.css', 'assets/css/style.css'],
				dest: 'style.css'
			}*/
		},
		uglify: { // Minificar os arquivos ".js".
			scripts: {
				src: [
					'bower_components/jquery.maskedinput/dist/jquery.maskedinput.min.js',
					'bower_components/bootstrap/dist/js/bootstrap.min.js',
					'bower_components/lightbox2/dist/js/lightbox.min.js',
					'bower_components/jarallax/dist/jarallax.min.js'
				],
				dest: 'assets/js/app.min.js'
			},
		},
		copy: {
			bootstrapFonts: {
				expand: true,
				cwd: 'bower_components/bootstrap/dist/fonts/',
				src: ['**'],
				dest: 'assets/fonts/',
				filter: 'isFile'
			},
			lightboxImages: {
				expand: true,
				cwd: 'bower_components/lightbox2/dist/images/',
				src: ['**'],
				dest: 'assets/images/',
				filter: 'isFile'
			}
		},
	});

	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-copy');

	grunt.registerTask('default', ['cssmin', 'concat', 'uglify', /*'jshint',*/ 'copy']);
}