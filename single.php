<?php get_header(); ?>
<section class="row">
<?php if(have_posts()):?>
	<header>
		<h1 class="col-md-12 title-upper"><?php the_title();?></h1>
		<?php cat_breadcrumb(); ?>
	</header>
	<section id="post-<?php the_ID(); ?>" <?php post_class('col-md-'.classCol()); ?>>
	<?php if(have_posts()): while(have_posts()): the_post(); ?>
		<?php the_content();?>
		<footer>
			<?php tag_breadcrumb(); ?>
		</footer>
		<?php post_nav(); ?>
	<?php endwhile; endif;?>
	</section>
	<?php get_sidebar( 'sidebar' ); ?>
<?php endif;?>
</section>
<?php get_footer(); ?>