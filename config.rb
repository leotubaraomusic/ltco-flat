css_dir = "" # Pasta para final "assets/css"
sass_dir = "scss"
images_dir = "assets/images"
output_style = :compressed
relative_assets = false
line_comments = false
sass_options = {:debug_info=>false}
sourcemap = false