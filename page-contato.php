<?php get_header(); ?>
<section id="contato" class="row">
<?php if(have_posts()):?>
	<header>
		<h1 class="col-md-12 title-upper"><?php the_title();?></h1>
	</header>
	<article <?php post_class('col-md-12'); ?>>
	<?php if(have_posts()): while(have_posts()): the_post(); ?>
		<?php if(get_field('frase_contato')): ?>
			<h2 class="col-md-8 col-md-offset-2 text-center">
				<?php the_field('frase_contato');?>
			</h2>
		<?php endif; ?>
		<article class="content">
			<?php the_content();?>
		</article>
		<?php if(get_field('email_contato')) : ?>
			<div class="separador">
				<span>OU</span>
			</div>
			<a href="mailto:<?php the_field('email_contato') ?>" target="_blank" class="contact-email">
				<i class="glyphicon glyphicon-envelope"></i>
				<?php if(get_field('email_contato_frase')) : ?>
				<span><?php the_field('email_contato_frase') ?></span>
				<?php endif; ?>
			</a>
		<?php endif; ?>
	<?php endwhile; endif;?>
	</article>
<?php endif;?>
</section>
<?php get_footer(); ?>